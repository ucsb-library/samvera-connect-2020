---
title: "Samvera Connect 2020"
date: 2020-01-13T12:02:10-08:00
draft: false
eventDates: "27–30 October"
location: "University of California, Santa Barbara"
---

Samvera Connect (hashtag #samvera2020) is a chance for the Samvera Community to
gather in one place at one time, with an emphasis on synchronizing efforts,
technical development, plans, and community links. The meeting program is aimed
at existing users, managers and developers and at new folks who may be just
“kicking the tires” on Samvera and who want to know more. Samvera advertises
this yearly conference with the slogan “as a Samvera Partner or user, if you can
only make it to one Samvera meeting this academic year,  this is the one to
attend!”